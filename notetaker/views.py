# views.py - файл представлений. Здесь мы будем указывать, что мы будем отдавать
# пользователю при переходе по тому или иному адресу в нашем приложении.
#
# Все адреса надо прописывать в объекте нашего приложения.
from flask import redirect
from flask import request

from notetaker import app, db
# Для отображения страницы следует использовать функцию render_template из
# flask
from flask import render_template

# Представление не может браться просто так, надо использовать данные из модели
from .models import Note
# Указание "При переходе по такому-то адресу открыть то-то" будут выглядеть примерно так:
# @{имя_переменной_приложения}.route('{путь_внутри_приложения}')
# Сразу после такого указания будет идти функция, которая запускается при переходе по этому
# адресу. Результатом работы такой функции будет текст веб-страницы.
@app.route('/')
def main_page():
    # Запрашиваем список всех записей и выводим его в шаблоне
    notes = Note.query.all()
    return render_template('note_list.html', notes=notes)

@app.route('/create_note')
def new_note():
    return render_template('note_edit.html')

@app.route('/save_note', methods=['POST'])
def save_note():
    note_name = request.form['note_name']
    note_text = request.form['note_contents']
    note = Note()
    note.update_with_name(note_name)
    note.update_with_text(note_text)
    db.session.add(note)
    db.session.commit()
    return redirect('/', code=302)

@app.route('/view_note/<note_id>')
def show_note(note_id):
    note = Note.query.filter_by(id=note_id).first()
    if note is None:
        return redirect('/', code=404)
    return render_template('note_view.html', note=note)
